#!/bin/bash
# set ENV:
# CFKEY=API-key
# CFUSER=username(email)
# CFZONE=zone-name
# CFHOST="host1-you-want-to-change host2-you-want-to-change"
# CFINTERVAL=300(second, default: 300)

function timestamp() {
	date "+%Y-%m-%dT%H:%M:%S%z"
}

function log() {
	echo "$(timestamp) $*"
}

function die() {
	log "$@"
	exit 1
}

function myip4() {
	dig +short +time=1 +tries=1 -4 myip.opendns.com a @resolver1.opendns.com
}

function myip6() {
	dig +short +time=1 +tries=1 -6 myip.opendns.com aaaa @resolver1.opendns.com
}

[ -z "${CFKEY}" ] && die "CFKEY is required"
[ -z "${CFUSER}" ] && die "CFUSER is required"

function getZoneID() {
	local zone_name="${1}"
	curl -s -X GET "https://api.cloudflare.com/client/v4/zones?name=$zone_name" \
		-H "X-Auth-Email: $CFUSER" \
		-H "X-Auth-Key: $CFKEY" \
		-H "Content-Type: application/json" | jq -r '.result[0].id'
}

[ -z "${CFZONE}" ] && die "CFZONE is required"
zone_identifier="$(getZoneID "${CFZONE}")"
[ -z "${zone_identifier}" ] && die "Get zone identifier failed"

function getRecordID() {
	local record_name="${1}"
	local record_type="${2:-A}"
	curl -s -X GET "https://api.cloudflare.com/client/v4/zones/$zone_identifier/dns_records?name=$record_name" \
		-H "X-Auth-Email: $CFUSER" \
		-H "X-Auth-Key: $CFKEY" \
		-H "Content-Type: application/json" | jq -r ".result[] | select(.type == \"${record_type}\") | .id"
}

[ -z "${CFHOST}" ] && die "CFHOST is required"
unset record_names
unset record_types
unset record_ids
i=0
for record in ${CFHOST}; do
	for rtype in A AAAA; do
		rid="$(getRecordID "${record}" "${rtype}")"
		if [ "${rid}" ]; then
			record_names[${i}]="${record}"
			record_types[${i}]="${rtype}"
			record_ids[${i}]="${rid}"
			((i++))
		fi
	done
done
(( i == 0 )) && die "No DNS record found for ${CFHOST}"

function setRecordIP() {
	local record_name="${1}"
	local record_type="${2}"
	local record_identifier="${3}"
	local ip="${4}"
	curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/$zone_identifier/dns_records/$record_identifier" \
		-H "X-Auth-Email: $CFUSER" \
		-H "X-Auth-Key: $CFKEY" \
		-H "Content-Type: application/json" \
		--data "{\"id\":\"$zone_identifier\",\"type\":\"$record_type\",\"name\":\"$record_name\",\"content\":\"$ip\"}"
}

oldip4=""
function checkIP4() {
	local ip
	ip="$(myip4)"
	if [ -z "${ip}" ]; then
		log "skip empty IPv4"
		return 0
	fi
	if [ "${ip}" == "${oldip4}" ]; then
		log "skip update ${CFHOST} A with the same IP ${ip}"
		return 0
	fi
	local i
	local update
	for i in "${!record_ids[@]}"; do
		if [ "${record_types[${i}]}" == "A" ]; then
			log "set ${record_names[${i}]} ${record_types[${i}]} IP to ${ip}"
			update="$(setRecordIP "${record_names[${i}]}" "${record_types[${i}]}" "${record_ids[${i}]}" "${ip}")"
			[ "$(echo "${update}" | jq '.success')" != "true" ] && die "${update}"
		fi
	done
	oldip4="${ip}"
	return 0
}

oldip6=""
function checkIP6() {
	local ip
	ip="$(myip6)"
	if [ -z "${ip}" ]; then
		log "skip empty IPv6"
		return 0
	fi
	if [ "${ip}" == "${oldip6}" ]; then
		log "skip update ${CFHOST} AAAA with the same IP ${ip}"
		return 0
	fi
	local i
	local update
	for i in "${!record_ids[@]}"; do
		if [ "${record_types[${i}]}" == "AAAA" ]; then
			log "set ${record_names[${i}]} ${record_types[${i}]} IP to ${ip}"
			update="$(setRecordIP "${record_names[${i}]}" "${record_types[${i}]}" "${record_ids[${i}]}" "${ip}")"
			[ "$(echo "${update}" | jq '.success')" != "true" ] && die "${update}"
		fi
	done
	oldip6="${ip}"
	return 0
}

while ((1)); do
	checkIP4
	checkIP6
	sleep "${CFINTERVAL:-300}"
done
