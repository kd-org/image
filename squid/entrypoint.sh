#!/bin/bash

set -eu

prepare_folders() {
  echo "Preparing folders..."
  mkdir -p /etc/squid/conf.d/
  mkdir -p /etc/squid/cert/
  mkdir -p /var/cache/squid/
  mkdir -p /var/log/squid/
  touch /etc/squid/conf.d/empty.conf
  chown -R squid:squid /etc/squid/cert/
  chown -R squid:squid /var/cache/squid/
  chown -R squid:squid /var/log/squid/
}

initialize_cache() {
  echo "Creating cache folder..."
  squid -z

  sleep 5
}

create_cert() {
  if [[ ! -f /etc/squid/cert/private.pem ]]; then
    echo "Creating certificate..."
    openssl req -new -newkey rsa:2048 -days 3650 -nodes -x509 \
      -keyout /etc/squid/cert/private.pem -out /etc/squid/cert/private.pem \
      -subj "/CN=${CN:-squid.local}/O=${O:-squid}/OU=${OU:-squid}/C=${C:-US}"
  else
    echo "Certificate found..."
  fi
}

clear_certs_db() {
  echo "Clearing generated certificate db..."
  rm -rfv /var/cache/squid/ssl_db/
  /usr/lib/squid/security_file_certgen -c -s /var/cache/squid/ssl_db -M 4MB
  chown -R squid.squid /var/cache/squid/ssl_db
}

run() {
  echo "Starting squid..."
  prepare_folders
  create_cert
  clear_certs_db
  initialize_cache
  exec squid -NYCd 1 -f /etc/squid/squid.conf
}

run
