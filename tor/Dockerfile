ARG VERSION=0.4.2.6
ARG ALPINE_IMAGE=alpine:3

FROM ${ALPINE_IMAGE} AS build
RUN apk add --no-cache build-base curl libevent-dev openssl-dev zlib-dev
WORKDIR /build
ARG VERSION
RUN curl -sLo tor.tar.gz "https://dist.torproject.org/tor-${VERSION}.tar.gz"
RUN tar xf tor.tar.gz
WORKDIR /build/tor-${VERSION}
RUN ./configure
RUN make
RUN make install

FROM ${ALPINE_IMAGE}
RUN apk add --no-cache libevent libgcc openssl zlib
RUN adduser -D -s /bin/sh -u 9876 tor
RUN mkdir /etc/tor
COPY --link tor/torrc /etc/tor/torrc
COPY --link --from=build /usr/local/bin /usr/local/bin
COPY --link --from=build /usr/local/share/tor /usr/local/share/tor
USER tor
RUN tor --help
CMD ["tor", "-f", "/etc/tor/torrc"]
